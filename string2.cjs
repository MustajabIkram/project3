
const string2 = (testString) => {

    // default value of returned array = [], in all corner/error cases;
    let resArr = [];

    if (testString && typeof testString === 'string') {
        testString = testString.trim();

        // splitting the array from . onwards
        let splittedArr = testString.split('.');
        
        // coverting to number, if valid
        for(let i = 0; i < splittedArr.length; i++) {
            if (!isNaN(Number(splittedArr[i]))) {
                resArr.push(Number(splittedArr[i]));
            } else {
                return [];
            }
        }
    }
    return resArr;
}

module.exports = string2;
