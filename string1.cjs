
const string1 = (testString) => {

    // default value of returned number = 0, in all corner/error cases;
    let number = 0;

    if (testString && typeof testString === 'string') {
        testString = testString.trim();

        // removing '$' without affecting the sign
        if (testString.charAt(0) === '$') {
            testString = testString.slice(1);
        } else if (testString.startsWith('-$') || testString.startsWith('+$')) {
            testString = testString.substring(0, 1) + testString.substring(2);
        }

        //remove all the , in the string
        while(testString.includes(',')) {
            testString = testString.replace(',' , '');
        }
        // coverting to number, if valid
        if (!isNaN(Number(testString))) {
            number = Number(testString);
        }
    }
    return number;
}

module.exports = string1;
