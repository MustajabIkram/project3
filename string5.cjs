const string5 = (testArr) => {
    let ans = "";
    if(testArr === undefined) return ans;
    if(testArr.length == 0) return ans;
    for(let i = 0; i < testArr.length; i++) {
        if(typeof testArr[i] != 'string') return "";
        ans += testArr[i];
        if(i < testArr.length - 1) ans += ' ';
    }
    ans += '.';

    return ans;
}

module.exports = string5;
