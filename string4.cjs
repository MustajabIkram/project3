const _ = require("underscore")

const string4 = (testObj) => {
    let name = "";
    let checkArr = _.keys(testObj);
    
    if(checkArr.length > 3) {
        return name;
    } else if(checkArr.length == 3) {
        if(checkArr[0] != 'first_name') return name;
        if(checkArr[1] != 'middle_name') return name;
        if(checkArr[2] != 'last_name') return name;
    } else if(checkArr.length == 2) {
        if(!(checkArr[0] == 'first_name' || checkArr[0] == 'middle_name')) return name;
        if(!(checkArr[1] == 'middle_name' || checkArr[1] == 'last_name')) return name;
    } else if(checkArr.length == 1) {
        if(!(checkArr[0] == 'first_name' || checkArr[0] == 'middle_name' || checkArr[0] == 'last_name')) return name;
    } else {
        return name;
    }

    let nameArr = _.values(testObj);
    for(let i = 0; i < nameArr.length; i++) {
        if(typeof nameArr[i] == "string") {
            name += nameArr[i][0].toUpperCase() + nameArr[i].slice(1).toLowerCase();
        } else return "";
        if(i < nameArr.length - 1) name += ' ';
    }
    

    return name;
}

module.exports = string4;
