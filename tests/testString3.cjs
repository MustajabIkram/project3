const string3 = require("../problem3.cjs");

const testString = "39/161/143";
const testString1 = "12/2/2022";
const testString2 = "15/04/1999";
const testString3 = "19/61/56";
const testString4 = "111/139s1/143";
const testString5 = "hi";

const result = string3(testString);
console.log(result);

const result1 = string3(testString1);
console.log(result1);

const result2 = string3(testString2);
console.log(result2);

const result3 = string3(testString3);
console.log(result3);

const result4 = string3(testString4);
console.log(result4);

const result5 = string3(testString5);
console.log(result5);