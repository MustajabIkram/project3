const string5 = require("../string5.cjs");

const testArr = ["the", "quick", "brown", "fox"];
const testArr1 = ["the", "quick", "brown"];
const testArr2 = ["the", "quick", 2, "fox"]
const testArr3 = [1, "brown", "fox"]
const testArr4 =  [1, 2, 5]
const testArr5 = [true, true, false, true]
const testArr6 = ["pi", 5]
const testArr7 = ["blue", "kite"]

const result = string5(testArr);
console.log(result);

const result1 = string5(testArr1);
console.log(result1);

const result2 = string5(testArr2);
console.log(result2);

const result3 = string5(testArr3);
console.log(result3);

const result4 = string5(testArr4);
console.log(result4);

const result5 = string5(testArr5);
console.log(result5);

const result6 = string5(testArr6);
console.log(result6);

const result7 = string5(testArr7);
console.log(result7);

const result8 = string5([]);
console.log(result8);