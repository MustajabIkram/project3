const string4 = require("../string4.cjs");

const testObject = {"first_name": "JoHN", "middle_name": "doe", "last_name": "SMith"};
const testObject1 = {"first_name": "JoHN", "last_name": "SMith"};
const testObject2 = {"middle_name": 2, "last_name": "SMith"};
const testObject3 = {"first_name": "pi", "middle_name": "johanna", "last_name": "larry"};
const testObject4 =  {"first_name": "larry", "last_name": "page"};
const testObject5 = {"first_name": "elon"}
const testObject6 = {"pi": 5}
const testObject7 = {"sji": "kite"}

const result = string4(testObject);
console.log(result);

const result1 = string4(testObject1);
console.log(result1);

const result2 = string4(testObject2);
console.log(result2);

const result3 = string4(testObject3);
console.log(result3);

const result4 = string4(testObject4);
console.log(result4);

const result5 = string4(testObject5);
console.log(result5);

const result6 = string4(testObject6);
console.log(result6);

const result7 = string4(testObject7);
console.log(result7);

const result8 = string4();
console.log(result8);