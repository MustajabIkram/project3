const string1 = require("../string1.cjs");

const testString = "test string";
const testString1 = "-$1,00222.4640";
const testString2 = "-$123";
const testString3 = "+$999i.6";
const testString4 = "$100.45";
const testString5 = "+$99.9";

const result = string1(testString);
console.log(result);

const result1 = string1(testString1);
console.log(result1);

const result2 = string1(testString2);
console.log(result2);

const result3 = string1(testString3);
console.log(result3);

const result4 = string1(testString4);
console.log(result4);

const result5 = string1(testString5);
console.log(result5);