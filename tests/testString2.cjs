const string2 = require("../string2.cjs");

const testString = "111.139.161.143";
const testString1 = "111.139.1h1.143";
const testString2 = "21.139.16.143";
const testString3 = "139.161.143.56";
const testString4 = "111.139.1s1.143";
const testString5 = "hi";

const result = string2(testString);
console.log(result);

const result1 = string2(testString1);
console.log(result1);

const result2 = string2(testString2);
console.log(result2);

const result3 = string2(testString3);
console.log(result3);

const result4 = string2(testString4);
console.log(result4);

const result5 = string2(testString5);
console.log(result5);