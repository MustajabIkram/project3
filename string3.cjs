
const string3 = (testString) => {

    // default value of returned array = [], contains date at 0 index, month at 1st index and year at 2nd index, in all corner/error cases;
    let dateArr = [];

    if (testString && typeof testString === 'string') {
        testString = testString.trim();

        // splitting the array from / onwards
        let splittedDate = testString.split('/');
        
        // coverting to number, if valid
        for(let i = 0; i < splittedDate.length; i++) {
            if (!isNaN(Number(splittedDate[i]))) {
                dateArr.push(Number(splittedDate[i]));
            } else {
                return 0;
            }
        }

        // Checking if date is valid
        if(splittedDate[0] < 1 || splittedDate[0] > 31) return 0; // if date is valid
        if(splittedDate[1] < 1 || splittedDate[1] > 12) return 0; // if month is valid
        if(splittedDate[2] < 1) return 0; // if year is valid
    }
    return dateArr[1];
}

module.exports = string3;
